unit Server;

interface

uses
  System.SysUtils,
  System.Classes,
  SynCrtSock;

type
  TServerlessServer = class
    private
      FServer: THTTPApiServer;
      function ProcessRequest(Ctxt: THttpServerRequest): cardinal;
    public
      constructor Create;
      destructor Destroy; override;
  end;

implementation

function TServerlessServer.ProcessRequest(Ctxt: THttpServerRequest): cardinal;
begin
  Ctxt.OutContent := 'Hello!';
  Result := 200;
end;

constructor TServerlessServer.Create;
begin
  Writeln('Starting server');
  FServer := THttpApiServer.Create(false);
  FServer.AddUrl('root','888',false,'+',true);
  FServer.OnRequest := ProcessRequest;
end;

destructor TServerlessServer.Destroy;
begin
  FreeAndNil(FServer);
end;

end.
