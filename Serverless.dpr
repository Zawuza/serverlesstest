program Serverless;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Server in 'src\Server.pas';

  var LServer: TServerlessServer;

begin
  try
    LServer := TServerlessServer.Create;
    ReadLn;
    FreeAndNil(LServer);
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
